import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;



public class Crossword {
    //declare variables
    //create a hashmape that has a coordinate and a char
    //private HashMap<Coordinate, Character> map = new HashMap<Coordinate, Character>(); 
    private ArrayList<String> wordsToFind = new ArrayList<String>();
    private Grid grid = new Grid();

    /*
    1. could change it to read a file or folder for many crossword puzzles
    2. File would contain many crossword puzzles
    3. each would need a name or a generated name if not given
    4. can have option to generate all answers from each file or a specific file name
    5. would nee option choice for 1) a folder with single file or all files or
       2) a single file input
    
    */

    public void loadPuzzle(String file) throws FileNotFoundException{ 
      File inputFile = new File(file+".txt");
      Scanner scnr = new Scanner(inputFile);
      int counter = 0;
      int gridHeight = 0;
      int gridLength = 0;

      //read each line while line exists, first line is grid
      //need to read grid length counter for the last lines
      //last lines are wordsToFind
      while(scnr.hasNextLine()){

        //read first line, then increase counter and set grid height
        if(counter == 0){
            //split by 'x'
            String[] gridLine = scnr.nextLine().split("x");
            gridLength = Integer.parseInt(gridLine[0]);
            gridHeight = Integer.parseInt(gridLine[1]);
            counter++;
        }
        else if(counter <= gridHeight){
            //read a line of letters
            String[] letters = scnr.nextLine().split(" ");
            int i;
            for(i=0; i<gridLength; i++){
                char letter = letters[i].charAt(0);//get the single character
                Coordinate coordinate = new Coordinate(counter-1, i);
                grid.addCoordinate(coordinate, letter);
               // System.out.println(grid.getLetterAtCoordinate(coordinate));
                //Coordinate newCoord = new Coordinate(0,0);
               // System.out.println(grid.getLetterAtCoordinate(newCoord));
            }
            counter++;
        }
        else{
            String wordToFind = scnr.nextLine();
            wordsToFind.add(wordToFind);
        }
      } 
      scnr.close();
    }

    //string function that will be the final string printed out from all of the java classes
    //will print out the word to find along with it's coordinates
    @Override
public String toString() {
        String r = "";

        for (String word : wordsToFind) {
            int wordLength = word.length();
            char startingLetter = word.charAt(0);
            char endingLetter = word.charAt(wordLength-1);

            Coordinate cord1 = grid.getCoordinateAtLetter(startingLetter);
            Coordinate cord2 = grid.getCoordinateAtLetter(endingLetter);
            r+= word + " " + cord1 + " " + cord2 + "\n";
            }
        return r;

        }

}
