import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Grid {
    private HashMap<Coordinate, Character> map = new HashMap<Coordinate, Character>(); 
    public void addCoordinate(Coordinate coordinate, Character letter){
        map.put(coordinate,letter);
    }

    //getter for letter at coordinate
    public Character getLetterAtCoordinate(Coordinate coordinate){
        Coordinate thisCoord = coordinate;
        // System.out.println("Getting Coordinate:" + thisCoord);
        // System.out.println("Getting letter: " + map.get(thisCoord));
        // return map.get(coordinate);
       // System.out.println("Map contents: " + map);
        Character letter = map.get(thisCoord);
        //System.out.println("Getting letter: " + letter);
        return letter;

    }

    //setter for letter at coordinate
    public void setLetterAtCoordinate(Coordinate coordinate, char letter){
        map.put(coordinate, letter);
    }

    //getter for coordinate object at letter
    public Coordinate getCoordinateAtLetter(char letter){
        for(Map.Entry<Coordinate, Character> entry: map.entrySet()){
            if(Objects.equals(letter, entry.getValue())){
                return entry.getKey();
            }
        }
        return null;
    }

}