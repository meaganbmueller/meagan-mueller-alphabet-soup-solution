import java.util.Scanner;
import java.lang.String;

//map the cross word display by mapping the entire grid
//mapgrid function to set grid and pass around the hashmap object
//needs to accept input
/* 
3x3
A B C
D E F
G H I
ABC
AEI
 */
/* 
ABC 0:0 0:2
AEI 0:0 2:2
 */ 
// broken up into file-reader/grid and answers
//main calls all parts



public class alphabetSoup {
  public static void main(String[] args) {
    //crossword puzzle object
    Crossword puzzle = new Crossword();

    //initialize scanner and get input file name and check for errors
    Scanner scnr = new Scanner(System.in);
    System.out.print("Please enter the input file name not including the extension(ie for input_file.txt please enter 'input_file'):");
    String file = scnr.nextLine();
    System.out.print("\n");


    //check for empty or incorrect format
    while(file.isEmpty()||file.contains(" ")||file.contains(".")){
      System.out.print("Please enter a valid input file name:");
      file = scnr.nextLine();
      System.out.print("\n");
    }
    scnr.close();

    //loadPuzzle from file
    try{
      puzzle.loadPuzzle(file);
    }catch(Exception e){
      e.printStackTrace();
    }

    //print toString for the puzzle output
    System.out.println(puzzle.toString());
    
  }
}