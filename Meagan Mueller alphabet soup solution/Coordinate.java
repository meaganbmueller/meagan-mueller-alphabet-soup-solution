//this class sets a coordinate object and set the coords for a row and column
//coord object returns a string of the "(row,cord)" format

import java.lang.String;

public class Coordinate {
    private int row;
    private int col;

    public Coordinate(int row, int col){
        this.row = row;
        this.col = col;
    }


    //getters and setters for row and column
    public int getRow(){
        return row;
    }

    public int getCol(){
        return col;
    }

    public void setRow(int row){
        this.row = row;
    }

    public void setCol(int col){
        this.col = col;
    }

    //String for the coord
    public String toString(){
        String r="";
        r+= "[" + this.row + "," + this.col + "]";
        return r;
    }

}
